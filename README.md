# Tarquin MRS with local (UiB) fixes 

## Background

The current-ish (2018) [upstream version](https://sourceforge.net/projects/tarquin/files/) of the Martin Wilson (and others)' wonderful free [Tarquin](http://tarquin.sourceforge.net/) MRS package currently has some lingering issues with correctly interpreting certain header values (particularly echo time, TE) for GE P-file format data from most contemporary scanner software revisions, as [discussed here](https://groups.google.com/forum/#!topic/tarquin_users_group/2oNlETrBSgw).

The present version addresses that issue, in [commit 3a160e6a](3a160e6a)

## Binary Versions

Compilation can be challenging, [particularly on MacOS](src/README.macOS.md). Therefore, pre-compiled binaries incorporating this fix have been made available:

For Linux: <https://drive.google.com/open?id=1yQcJVcPHvgKsPhFySxCoQ0_w1lZMCOIc>

For MacOS (10.14): <https://drive.google.com/open?id=10MCZs40dkVJ8fGVqmwQT4VPNcpkQ2i1A>

Note that the MacOS binaries are unfortunately command-line only... there are some archaic Qwt5 dependencies which I never did manage to resolve. If you're using the GUI, it's easy enough to correct the TE value in the load dialog anyway...

## Further Details

See the [original documentation in the src folder](src/)
