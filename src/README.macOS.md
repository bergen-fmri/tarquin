# Building on MacOS

This is a rough guide. It worked for me, mostly (still doesn't work for tarquingui component). Your mileage may well vary.

## Gather the dependencies

The least painful approach to building on MacOS probably involves the use of
Homebrew to expediate handling of package dependencies.

Install homebrew like this:

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Then we need to include:

### cmake

(see also https://cmake.org/download/)

```
brew install cmake
```

### Google protocol buffers:

```
brew install protobuf
```

#### But...

The current protocol buffer implementation makes use of the c++11 standard, which is not the default on MacOS. Tell cmake to tell cxx to use c++11 standard, by appending this to the cmake command line: `-D CMAKE_CXX_FLAGS="-std=c++11`


### boost

```
brew install boost
```

### gfortran

Via homebrew, this comes along with gcc:

```
brew install gcc
```

### qt 4.x

Tarquin requires 4.x, homebrew typically installs 5.x, so use homebrew tap to get a 4.x release:

```
brew tap cartr/qt4
brew tap-pin cartr/qt4
brew tap-pin qt@4
```

#### But...

cmake still doesn't find the right version, so we need to explicitly tell cmake where it is, by appending to the command line: `-D QT_QMAKE_EXECUTABLE="/usr/local/opt/qt@4/bin/qmake"`

Some of these defs may or may not be helpful:

```
export PATH="/usr/local/opt/qt@4/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/qt@4/lib"
export CPPFLAGS="-I/usr/local/opt/qt@4/include"
```

### fftw

Although the pre-flight checks via cmake don't tell us, fftw and qwt are also required.

```
brew install fftw 
```

### qwt

```
brew install qwt
```

#### But...

cxx doesn't find this, so tell cmake about it explicitly by appending this to the command line: `-I/usr/local/opt/qwt/lib/qwt.framework/Versions/6/Headers"`

#### But...

Tarquin uses an old 5.x version of qwt. Current (6.x) releases don't define QwtDoublePoint, which in any rate is just a wrapper for QPointF for Qt 4.x+

So, work around this by removing #include QwtDoubleRect.h and replace all instances of QwtDoublePoint with QPointF.

## Proceed with cmake and make 

So, forcing the right QT (4.x), c++11 standard and qwt header path from the command line, we have:

```
cmake -D QT_QMAKE_EXECUTABLE="/usr/local/opt/qt@4/bin/qmake" -D CMAKE_CXX_FLAGS="-std=c++11 -I/usr/local/opt/qwt/lib/qwt.framework/Versions/6/Headers" ..
```

#### But...

This will build the standalone command-line components, but still doesn't work for tarquingui. Seem to be some lingering QT version issues :(
